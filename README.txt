Moleculix++ 
============================================
Moleculix++ is a library for molecular visualization and generation of molecular geometric related information. 
The library provide logic for interactive manipulation of 3D objects, such as atoms, bonds, molecular fragments etc.

Please see www.moleculix.com for info, updates and the latest code.

Copyright Dune Scientific, LLC 1995-2015, M. T. Karlsson
